import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter } from 'react-router-dom'

class Screens extends Component {

    constructor( props ) {
        super( props );
        this.state = {
            isLoading: false
        };
    }

    getChildContext() {
        const t = this;
        return {
            screens: {
                load( component ) {
                    return t.load( component )
                },
            }
        }
    }

    load( component ) {
        this.setState( { isLoading: true } );
        return new Promise( ( resolveHandle, errorHandle ) => {
            if ( !component ) {
                this.setState( { isLoading: false } );
                resolveHandle( null );
            }
            else {
                const type = typeof component;
                if ( type == 'string' ) {
                    if ( !Screens.options.load ) {
                        this.setState( { isLoading: false } );
                        throw new Error( '`Screens.loader()` is not defined' );
                    }

                    Screens.options.load( component ).then( ( module ) => {
                        this.setState( { isLoading: false } );
                        resolveHandle( module.default );
                    } ).catch( ( error ) => {
                        console.log( 'error!', error );
                        this.setState( { isLoading: false } );
                        errorHandle( error );
                    } );
                }
                else {
                    resolveHandle( component );
                    //this.setState( { loaded: true } );
                    //this.context.screens.setLoading( false );
                }
            }
        } );
    }

    render() {
        const { isLoading } = this.state;

        const {
            children,
            basename,
            getUserConfirmation,
            forceRefresh,
            keyLength,
        } = this.props;

        return (
            <BrowserRouter
                basename={basename}
                getUserConfirmation={getUserConfirmation}
                forceRefresh={forceRefresh}
                keyLength={keyLength}
            >
                <div>
                    {isLoading && Screens.options.loader()}
                    {children}
                </div>
            </BrowserRouter>
        );
    }
}

Screens.childContextTypes = {
    screens: PropTypes.object
};

Screens.options = {
    load: null,
    loader() {
        return <div>Loading...</div>
    }
};

Screens.config = function ( opts ) {
    Screens.options = Object.assign( Screens.options, opts );
};

Screens.propTypes = {
    basename: PropTypes.string,
    getUserConfirmation: PropTypes.func,
    forceRefresh: PropTypes.bool,
    keyLength: PropTypes.number,
    children: PropTypes.node,
    screensDir: PropTypes.string,
};

export default Screens;