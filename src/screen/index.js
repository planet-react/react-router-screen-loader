import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types';

class Screen extends Component {
    constructor( props, context ) {
        super( props, context );

        this.state = {
            loaded: false
        };
        this.component = null;
    }

    componentDidMount() {
        this.loadComponent( this.props.component );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.component !== this.props.component ) {
            this.loadComponent( nextProps.component );
        }
    }

    loadComponent( component ) {
        this.setState( { loaded: false } );

        this.context.screens.load( component ).then( ( c ) => {
            this.component = c;
            this.setState( { loaded: true } );
        } ).catch( ( error ) => {
            console.log( 'ERROR', error );
            this.setState( { loaded: true } );
        } );

        // if ( !component ) {
        //     this.component = null;
        //     this.setState( { loaded: true } );
        //
        //     this.context.screens.setLoading( false );
        // }
        // else {
        //     const type = typeof component;
        //     if ( type == 'string' ) {
        //         if ( !Screen.options.load ) {
        //             throw new Error( '`Screen.loader()` is not defined' );
        //         }
        //
        //         Screen.options.load( component ).then( ( module ) => {
        //             this.component = module.default;
        //             this.setState( { loaded: true } );
        //
        //             this.context.screens.setLoading( false );
        //         } ).catch( ( error ) => {
        //             console.log( 'error!', error );
        //             this.context.screens.setLoading( false );
        //         } );
        //     }
        //     else {
        //         this.component = component;
        //         this.setState( { loaded: true } );
        //
        //         this.context.screens.setLoading( false );
        //     }
        // }
    }

    render() {
        const { loaded } = this.state;
        const { render, children, path, exact, strict, location } = this.props;
        if ( loaded == true ) {
            return (
                <Route
                    render={render}
                    path={path}
                    exact={exact}
                    strict={strict}
                    location={location}
                    component={this.component}
                >
                    {children}
                </Route>
            )
        } else {
            //return ( this.context.screens.loader() )
            return ( <div/> )
        }
    }
}

Screen.contextTypes = {
    screens: PropTypes.object
};

Screen.propTypes = {
    component: PropTypes.object,
    render: PropTypes.func,
    children: PropTypes.func,
    path: PropTypes.string,
    exact: PropTypes.bool,
    strict: PropTypes.bool,
    location: PropTypes.object,
};

export default withRouter( Screen );