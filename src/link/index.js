import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink, withRouter } from 'react-router-dom'

class Link extends Component {

    constructor( props ) {
        super( props );

        this.to = props.to;
        this.match = props.match;
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.path != this.props.path ) {
            this.to = nextProps.to;
            this.match = nextProps.match;
        }
    }

    navigate() {
        const { history, to } = this.props;
        history.push( to );
    }

    render() {
        const { to, exact, children, isActive, onClick, role, ariaLabel,
            className, target } = this.props;

        return (
            <NavLink
                exact={exact}
                to={to}
                isActive={isActive}
                role={role}
                aria-label={ariaLabel}
                className={className}
                onClick={event => {
                    event.preventDefault();

                    if ( target && target == '_blank' ) {
                        window.open( this.to );
                        return;
                    }

                    this.navigate();

                    if ( onClick ) {
                        onClick( event );
                    }
                }}
            >
                {children}
            </NavLink>
        );
    }
}

Link.contextTypes = {
    screens: PropTypes.object
};

Link.propTypes = {
    to: PropTypes.string.isRequired,
    exact: PropTypes.bool,
    className: PropTypes.string,
    isActive: PropTypes.func,
    onClick: PropTypes.func,
    onBlockedClick: PropTypes.func,
    target: PropTypes.string,
    children: PropTypes.node,
};

export default withRouter( Link );